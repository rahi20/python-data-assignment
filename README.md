# Python Data Assignment 

## Installing and starting the app 
Requires : 
- Python 3.7 or higher.
#### - Install pipenv on your global python setup
```Python
    pip install pipenv 
```
Or follow [documentation](https://pipenv.pypa.io/en/latest/install/) to install it properly on your system
#### - Install requirements
```sh
    cd python-data-assignement && pipenv install && pipenv shell 
```

#### - Start the application
```sh
    sh run.sh
```
- API : http://localhost:5000
- Streamlit Dashboard : http://localhost:8000



## Refactoring

To make the streamlit app maintainable, I boke it into modules, i.e made it modular.  

 
To add a new page, in the file `pages.yaml` add the name/title you want shown in the sidebar options and the name of the file without the extension. 


Example :
```yaml
package: src.scripts # points to folder src/scripts
EDA: eda
Training: training
Inference: inference
Sub : sub_pack.test # -> src/scripts/sub_pack.test.py
```

**The colon should be followed by whitespace, otherwise you'll get unexpected behavior.**

The `package` points to the folder where to store the modules and sub packages.

## EDA

### Check The EDA on the app

## Training 

For convnience, I created a list of checkboxes each one referring to one of the available models.

![](./input/static/models_ckbx.png)

You can combine multiple models if you'd like to.


The names of the modules containing the models are stored in the `src/models/classifiers.yaml`. It contains the names of the models and their corresponding module name.

Example : 
```yaml
DecisionTreeModel: decision_tree_model
LogisticRegressionModel: logistic_regression
GaussianNBModel: gaussian_nb
SVCModel: svc_model
RandomForestModel: random_forest
MLPClassifierModel: mlp_model
```

<br>

To add a new model, just add its name (Name of the class) and the name of the file (without the extension)   

<br>

The best F1 score obtained so far is of the **RandomForestModel** and that of the **MLPClassifier** (0.85+)

## Inference

We create a Model `PredModel` to define the structure of the table. We define the `insert` method to insert a row in the table `predictions` and `fetch_data` method to fetch all the rows from the table. 