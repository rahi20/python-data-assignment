import sqlalchemy as sqla
# from sqlalchemy_utils import database_exists
from src.constants import INFERENCE_EXAMPLE, DBS_PATH
import pandas as pd

from sqlalchemy import Column, Integer, String, Float
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base


db = sqla.create_engine('sqlite:///' + str(DBS_PATH) + '/preds.db')
Base = declarative_base()

Session = sessionmaker(bind = db)
dbsession = Session()


class PredModel(Base):
    
    #table_name
    __tablename__ = "predictions"
    
    #columns
    id = Column(Integer, primary_key=True, autoincrement= True)
    f_11 = Column(Float)
    f_13 = Column(Float)
    f_15 = Column(Float)
    amount = Column(Float)
    label = Column(String)


    def insert(self):
        dbsession.add(self)
        dbsession.commit()

    @staticmethod
    def fetch_data() -> pd.DataFrame:
        return pd.read_sql(dbsession.query(PredModel).order_by(PredModel.amount).statement, dbsession.bind)

Base.metadata.create_all(bind=db)





    # def __init__(self, SQLALCHEMY_DB_URI : str) -> None:
    #    self.SQLALCHEMY_DB_URI = SQLALCHEMY_DB_URI
    #    self.db = sqla.create_engine(self.SQLALCHEMY_DB_URI, echo = True)


    # def create_db(self, replace = False) ->  sqla.engine.Engine : 

    #     if not database_exists(self.SQLALCHEMY_DB_URI) :
    #         n = len(INFERENCE_EXAMPLE)
    #         meta = sqla.MetaData()
    #         columns = [sqla.Column('V' + str(i), sqla.Float) for i in range(n - 2)]
    #         columns.append(sqla.Column('Amount', sqla.Float))
    #         columns.append(sqla.Column('Label', sqla.String))

    #         predictions = sqla.Table(
    #             'predictions', meta, *columns
    #         )
    #         meta.create_all(self.db)
        
    #     return self.db


    # def insert_one(self) -> None :
    #     dbsession = sessionmaker(bind = self.db)


    # @staticmethod
    # def fetch_db_data() -> pd.DataFrame :
    #     preds = 
    #     return