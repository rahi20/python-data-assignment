from flask import Blueprint, request, jsonify
from src.constants import AGGREGATOR_MODEL_PATH
from src.models.aggregator_model import AggregatorModel
import numpy as np
import pandas as pd

import flask_sqlalchemy as sqla
from src.api.Model import PredModel
from flask_sqlalchemy import SQLAlchemy


from src.constants import DBS_PATH

model = AggregatorModel()
model.load(AGGREGATOR_MODEL_PATH)
blueprint = Blueprint('api', __name__, url_prefix='/api')


@blueprint.route('/')
@blueprint.route('/index')
def index():
    return "CARD FRAUD DETECTION API - INFERENCE BLUEPRINT"


@blueprint.route('/inference', methods=['POST'])
def run_inference():
    #The api is not getting the right data as it's supposed to
    if request.method == 'POST':
        features = np.array(request.json()).reshape(1, -1)
        prediction = model.predict(features)

        label = "Clear" if str(prediction[0]) == "0" else "Fraudulent"
        pred = PredModel(f_11 = features[11], f_13 = features[13], f_15 = features[15], amount = features[28], label = label)
        pred.insert()
        
        return str(prediction[0])
        #return prediction[0]


@blueprint.route("/fetch-data", methods = ['POST'] )
def featch_data():
        return PredModel.fetch_data().to_dict()

