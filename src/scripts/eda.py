import os
import streamlit as st
import pandas as pd  

import plotly.express as px


import matplotlib.pyplot as plt
import seaborn as sns
import io
from src.scripts.utils import PlotUtils, ReadUtils
from src.constants import DATASET_PATH, OUTPUT_PATH

sns.set_theme()

def content() -> None:
    
    
    df = ReadUtils.read_csv(DATASET_PATH)

    st.header("Exploratory Data Analysis")

    n_rows = df.shape[0]

    st.text("Let's check the data first : ")

    st.write("""Number of rows and columns\n 
        rows : {0}\n
        columns : {1}""".format(df.shape[0], df.shape[1]))

    st.text("First 5 rows of the dataset : ")
    st.dataframe(df.head())

    st.text("The description of the data : ")
    st.dataframe(df.describe())

    st.text("Looks like the dataset doesn\'t have any missing values")
    
    #df.dtypes not working, depd issue
    info = io.StringIO()
    df.info(buf=info)
    st.text(info.getvalue())


    st.text("Let's look at the distribution of classes")
    
    h_fraud = df['Class'].sum()
    h_not_fraud = n_rows - h_fraud

    bar_data = {
        "class" : ["Not Fraud", "Fraud"],
        "transactions" : [h_not_fraud, h_fraud]
    }

    fig = PlotUtils.bar_chart(pd.DataFrame.from_dict(bar_data), x = "class", y="transactions")
    #fig = px.bar(pd.DataFrame.from_dict(bar_data), x = "class", y = "transactions")
    st.plotly_chart(fig)

    st.write( '%.2f%% of transactions are not fraudulant, which means the dataset is very unbalanced' % ((h_not_fraud/n_rows)*100) )
    st.write("What we can do to solve this is resampling to balance the data, either by undersampling; meaning reducing the size of the non-fraudulant class or by oversampling which means increasing the size of the fraudulant class")


    st.write("Let's see now the changes in the amount in both fraudulent and non fraudulent transactions")
   
    df_fraud = df[df['Class'] == 1]
    df_no_fraud = df[df['Class'] == 0]
    
    fig = PlotUtils.sub_plots([df_fraud, df_no_fraud], x = "Time", y = "Amount", titles = ("Amounts of fraudulant transactions against time", "Amounts of non fraudulant transactions over time"))
    st.plotly_chart(fig)


    st.write("**Box Plots**")
    fig = PlotUtils.box_plot(df, x = "Class", y = "Amount")
    st.plotly_chart(fig)

    corr = df.corr()
    st.write("**Correlation matrix**")
    
    if os.path.exists(str(OUTPUT_PATH) + "/plots/corr_matrix.png") :
        st.image(str(OUTPUT_PATH) + "/plots/corr_matrix.png")
    else :
        fig = PlotUtils.corr_matrix(corr)
        st.pyplot(fig)

    



# What can be improved, use images instead of plotly for performance
# It's still slow even with the cache 