from typing import Tuple, List

import streamlit as st
import pandas as pd
import plotly.express as px
from plotly.subplots import make_subplots
import plotly.graph_objects as go
import matplotlib.pyplot as plt
import seaborn as sns

from src.constants import OUTPUT_PATH

class PlotUtils:

    @st.cache
    def bar_chart(df : pd.DataFrame, x: str, y: str):
        fig = px.bar(df, x = x, y = y)
        return fig


    @st.cache
    def sub_plots(dfs: List[pd.DataFrame], x, y, titles : Tuple[str], cols: int = 2, height: int = 600, width: int = 850):
        
        fig = make_subplots(
            rows = 1, 
            cols = cols,
            subplot_titles= titles
        )
        i = 1
        for data in dfs :
            fig.add_trace(
                go.Scatter(x = data[x], y = data[y]),
                row = 1, col = i
            ) 
            i += 1
        
        fig.update_layout(height = height, width = width, showlegend = False)

        return fig
    
    @st.cache
    def box_plot(df: pd.DataFrame, x: str, y: str):
        return px.box(df, x = x, y = y)


    def corr_matrix(corr: pd.DataFrame):
        fig = plt.figure(figsize=(14, 14))
        sns.heatmap(corr,xticklabels=corr.columns,yticklabels=corr.columns,linewidths=.1,cmap="Reds")
        plt.savefig(str(OUTPUT_PATH) + "/plots/corr_matrix.png")
        return fig

class ReadUtils:

    @st.cache
    def read_csv(path : str) -> pd.DataFrame:
        return pd.read_csv(path)