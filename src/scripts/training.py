import yaml 
import time
from pandas._config.config import options
import streamlit as st
from PIL import Image

from src.constants import CM_PLOT_PATH, CLASSIFIERS_PATH
from src.training.train_pipeline import TrainingPipeline


def content() -> None :

    st.header("Model Training")

    #t.info("Before you proceed to training your model. Make sure you "
    #        "have checked your training pipeline code and that it is set properly.")

    st.info("Choose one classifier or more you'd like to train (Model Aggregation)."
    "If you choose none, The DecisionTree is trained by default.")

    
    #Using checkboxes instead of a selectbox because we use multiple models
    
    # Get the names of the models defined
    #  

    with open(CLASSIFIERS_PATH / "classifiers.yaml") as classifiers_file:
        classifiers = yaml.load(classifiers_file, Loader=yaml.FullLoader)

        #file_name = tuple(classifiers.values())
        clf_names = tuple(classifiers.keys())

        # create a dict to check whether a checkbox was checked or not 
        
        is_checked = {key : False for key in clf_names}

        for clf in clf_names:
            is_checked[clf] = st.checkbox(clf)

        #st.write(is_checked)
        st.write(" ")


        name = st.text_input('Model Name', placeholder='decisiontree')
        print("ll", name)
        serialize = st.checkbox('Save Model')
        train = st.button('Train Model')

        if train:
            with st.spinner('Training model, please wait...'):
                time.sleep(1)
                try:
                    clfs_checked = [key for key in clf_names if is_checked[key]]
                    st.write(clfs_checked)
                    
                    tp = TrainingPipeline(clfs_checked)
                    tp.train(serialize=serialize, model_name=name)
                    tp.render_confusion_matrix(plot_name=name)
                    accuracy, f1 = tp.get_model_perfomance()
                    col1, col2 = st.columns(2)

                    col1.metric(label="Accuracy score", value=str(round(accuracy, 4)))
                    col2.metric(label="F1 score", value=str(round(f1, 4)))

                    st.image(Image.open(CM_PLOT_PATH))

                except Exception as e:
                    st.error('Failed to train model!')
                    st.exception(e)