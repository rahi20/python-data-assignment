from os import listdir
from pandas.io import sql
from requests.api import head
import streamlit as st
import requests
import time
import pandas as pd
import numpy as np

from src.constants import INFERENCE_EXAMPLE, DBS_PATH


def content() -> None:
    st.header("Fraud Inference")

    st.info("This section simplifies the inference process. "
            "You can tweak the values of feature 1, 2, 19, "
            "and the transaction amount and observe how your model reacts to these changes.")

    feature_11 = st.slider('Transaction Feature 11', -10.0, 10.0, step=0.001, value=-4.075)
    feature_13 = st.slider('Transaction Feature 13', -10.0, 10.0, step=0.001, value=0.963)
    feature_15 = st.slider('Transaction Feature 15', -10.0, 10.0, step=0.001, value=2.630)
    amount = st.number_input('Transaction Amount', value=1000, min_value=0, max_value=int(1e10), step=100)
    
    infer = st.button('Run Fraud Inference')
    list_preds = st.button("List Predictions")

    INFERENCE_EXAMPLE[11] = feature_11
    INFERENCE_EXAMPLE[13] = feature_13
    INFERENCE_EXAMPLE[15] = feature_15
    INFERENCE_EXAMPLE[28] = amount

    if infer :
        with st.spinner('Running inference...'):
            time.sleep(1)
            headers = {'Content-Type': 'application/json', 'Accept':'application/json'}
            try:
                result = requests.post(
                    'http://localhost:5000/api/inference',
                    json=INFERENCE_EXAMPLE,
                    headers = headers
                )
                label = ""
                if int(int(result.text) == 1):
                    st.success('Done!')
                    st.metric(label="Status", value="Transaction: Fraudulent")
                    label = "fraudulent"
                else :
                    st.success('Done!')
                    st.metric(label="Status", value="Transaction: Clear")
                    label = "clear"
                
            except Exception as e:
                st.error('Failed to call Inference API!')
                st.exception(e)

    if list_preds :
        with st.spinner('Getting past predictions...'):
            try :
                preds_table= requests.post(
                        'http://localhost:5000/api/fetch-data'
                    )
                #st.write(preds_table.json())
                st.dataframe(pd.DataFrame.from_dict(preds_table.json()).head(n = 20))
                

            except Exception as e:
                st.error('Failed to call Predictions API!')
                st.exception(e)



#result = res.json()["label"]
                #preds_table = res.json()["preds"]

                # label = ""
                # if int(int(result.text) == 1):
                #     st.success('Done!')
                #     st.metric(label="Status", value="Transaction: Fraudulent")
                #     label = "fraudulent"
                # else :
                #     st.success('Done!')
                #     st.metric(label="Status", value="Transaction: Clear")
                #     label = "clear"
                
                #SQLALCHEMY_DB_URI = 'sqlite:///' + str(DBS_PATH) + "/predictions.db"

                #we'll store the predictions locally the predictions
                #db = PredModel.create_db(SQLALCHEMY_DB_URI) #it doesn't create it again if it already exists 
                #pred_col = PredModel(f_11 = feature_11, f_13 = feature_13, f_15 = feature_15, amount = amount, label = label)
                #pred_col.insert()
                #st.write("Done")
                # Print the table
                #st.dataframe(PredModel.fetch_data())
               
                #Print the predictions table :
                #st.write("**Predictions Table**")
                #st.dataframe(pd.DataFrame.from_dict(preds_table.json()).head(n = 20))                
                #st.write(result.json())

                #st.write(preds_table.json())
