from sklearn.ensemble import RandomForestClassifier

from src.models.base_model import BaseModel

class RandomForestModel(BaseModel):
    def __init__(self, n_jobs : int = 4, random_state : int =42, criterion : str = 'gini', n_estimators = 100, **kwargs):
        super().__init__(
            model=RandomForestClassifier(
                n_jobs = n_jobs, 
                random_state = random_state, 
                criterion = criterion, 
                n_estimators = n_estimators, 
                **kwargs)
        )
