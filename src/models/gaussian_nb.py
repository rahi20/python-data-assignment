from sklearn.naive_bayes import GaussianNB

from src.models.base_model import BaseModel

class GaussianNBModel(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(
            model=GaussianNB(**kwargs)
        )
