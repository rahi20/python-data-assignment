from sklearn.svm import SVC

from src.models.base_model import BaseModel


class SVCModel(BaseModel):
    def __init__(self, C : int = 10, **kwargs):
        super().__init__(
            model=SVC(C = C , **kwargs)
        )
