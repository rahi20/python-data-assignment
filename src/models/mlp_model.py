from sklearn.neural_network import MLPClassifier

from src.models.base_model import BaseModel

class MLPClassifierModel(BaseModel):
    def __init__(self, solver : str = 'adam', random_state : int = 42, tol: float = 1e-4, max_iter: int = 200, early_stopping: bool = True, hidden_layer_sizes : tuple =(20, 10),**kwargs):
        super().__init__(
            model=MLPClassifier(
                solver = solver, 
                random_state = random_state,
                tol = tol,
                max_iter = max_iter,
                early_stopping = early_stopping,
                **kwargs)
        )
