#!/bin/sh

#Run API
export FLASK_APP="src/api/app.py"
echo $FLASK_APP
API_PID_FILE=pid_files/api_pid.txt
if test -f "$API_PID_FILE";
then
    pid=`cat "$API_PID_FILE"`
    echo $pid
    if [ "$pid" != '' ]
    then
        kill -9 $pid
        rm "$API_PID_FILE"
    fi
    echo "RUNNING INFERENCE API"
    nohup sh -c 'flask run' 2>&1 > logs/api.log &
    #echo "RUNNING INFERENCE API 2"
    echo $! > "$API_PID_FILE"
fi

#Run Streamlit
STREAMLIT_PID_FILE=pid_files/streamlit_pid.txt
if test -f "$STREAMLIT_PID_FILE";
then
    pid=`cat "$STREAMLIT_PID_FILE"`
    echo $pid
    if [ "$pid" != '' ]
    then
        kill -9 $pid
        rm "$STREAMLIT_PID_FILE"
    fi
    echo "RUNNING STREAMLIT DASHBOARD"
    nohup sh -c 'streamlit run dashboard.py --server.port 8000' 2>&1 > logs/streamlit.log &
    echo $! > "$STREAMLIT_PID_FILE"
fi