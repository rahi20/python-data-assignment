import yaml
import importlib
import streamlit as st

st.set_page_config(page_title="Card Fraud Detection Dashboard", layout="centered")


st.sidebar.title("DATA THEMES")

with open('pages.yaml') as pages_file:
    pages = yaml.load(pages_file, Loader=yaml.FullLoader)
    
    base_pack = pages['package']
    options = tuple(pages.keys())[1:]
    names = tuple(pages.values())[1:]

    sidebar_option = st.sidebar.selectbox(
        "OPTIONS",
        options = options
    )
    page = importlib.import_module( base_pack + "." + pages[sidebar_option])
    page.content()